<?php require_once 'include/header.php' ?>
<?php require_once 'include/nav.php' ?>
<form action="" method="post">
<fieldset>
    <legend>Login as Existing User</legend>
    <div>
        <label for="username">Username:</label>
        <input type="text" name="username" id="username">
    </div>
    <div>
        <label for="password">Password:</label>
        <input type="password" name="pass" id="pass">
    </div>
    <div>
        <label for="remember">Remember me: <input type="checkbox" name="remember" id="remember"></label>
    </div>
    <div>
        <button type="submit">Login</button>
    </div>
</fieldset>
</form>
<?php require_once 'include/footer.php' ?>