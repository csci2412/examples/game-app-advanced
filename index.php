<?php require_once 'include/header.php' ?>
<h1>My games</h1>
<nav>
    
<?php if ($model->isLoggedIn): ?>
    <span>Hello, <?=$_SESSION['username']?>!</span>
    <a href="game-form.php">New game</a>
    <a href="logout.php">Logout</a>
<?php else: ?>
    <a href="register.php">Register</a>
    <a href="login.php">Login</a>
<?php endif; ?>
</nav>

<?php if ($model->isLoggedIn):?>
<table>
    <tr>
        <th>Box art</th>
        <th>Title</th>
        <th>Release year</th>
        <th>System</th>
        <th>Developer</th>
        <th>Categories</th>
        <th>Completed</th>
        <th>Actions</th>
    </tr>
    <?php foreach($model->games as $game): ?>
    <tr>
        <td>
        <?php if($game->imageId): ?>
            <img src="get-image.php?id=<?=$game->imageId?>" alt="No box art" height="100">
        <?php endif; ?>
        </td>
        <td><?=$game->title?></td>
        <td><?=$game->releaseYear?></td>
        <td><?=$game->systemName?></td>
        <td><?=$game->developerName?></td>
        <td><?=$game->categories?></td>
        <td><?=$game->completed ? 'Yes' : 'No'?></td>
        <td>
            <a href="game-form.php?id=<?=$game->id?>">Edit</a>
            <a href="delete-game.php?id=<?=$game->id?>"
                onclick="return confirm('Are you sure?')">Delete</a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php else: ?>
<p>Login to create your game collection</p>
<?php endif; ?>
<?php require_once 'include/footer.php';