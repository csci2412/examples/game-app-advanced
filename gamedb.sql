drop database if exists gamedb;
create database gamedb;
use gamedb;

create table `user`(
    `id` int unsigned auto_increment primary key,
    `username` nvarchar(255) not null,
    `password` char(60) not null,
    unique(`username`)
);

create table `token`(
    `id` int unsigned auto_increment primary key,
    `token` char(64) not null,
    `user_id` int unsigned not null,
    foreign key (`user_id`) references `user`(`id`) on delete cascade
);

create table `game`(
	`id` int unsigned auto_increment,
    `user_id` int unsigned not null,
    `title` nvarchar(255) not null,
    `release_year` year not null,
    `completed` bool default false,
    `developer_id` int unsigned not null,
    `system_id` int unsigned not null,
    primary key(`id`)
);

create table `developer`(
	`id` int unsigned auto_increment,
    `name` nvarchar(255) not null,
    `country` nvarchar(255),
    `founded_year` year,
    primary key(`id`)
);

create table `system`(
	`id` int unsigned auto_increment,
    `name` nvarchar(255) not null,
    `release_year` year,
    primary key(`id`)
);

create table `category`(
	`id` int unsigned auto_increment,
    `name` nvarchar(255) not null,
    primary key(`id`)
);

create table `game_category`(
	`game_id` int unsigned,
    `category_id` int unsigned,
    primary key(`game_id`, `category_id`)
);

create table `image`(
    `id` int unsigned auto_increment primary key,
    `filename` nvarchar(255) not null,
    `filetype` nvarchar(50) not null,
    `filedata` mediumblob,
    `game_id` int unsigned not null,
    foreign key (`game_id`) references `game`(`id`)
        on delete cascade
);

alter table `game` add constraint `game_user_fk`
    foreign key (`user_id`) references `user`(`id`);
alter table `game` add constraint `game_system_fk`
	foreign key (`system_id`) references `system`(`id`);
alter table `game` add constraint `game_developer_fk`
	foreign key (`developer_id`) references `developer`(`id`);

alter table `game_category` add constraint `gc_game_fk`
	foreign key (`game_id`) references `game`(`id`)
    on delete cascade;
alter table `game_category` add constraint `gc_category_fk`
	foreign key (`category_id`) references `category`(`id`) 
	on delete cascade;

insert into `category`(`name`) values ('RPG'), ('Strategy'), 
	('Platformer'), ('Adventure'), ('Puzzle'), ('Action');

insert into `system`(`name`) values ('NES'), ('SNES'),
	('Genesis'), ('PSX');

insert into `developer`(`name`) values ('Nintendo'),('Squaresoft'),
	('Capcom'),('Konami'), ('Sega');
/*    
insert into `game`(`title`,`release_year`,`completed`,`system_id`,
	`developer_id`) values 
    ('Super Mario Bros', 1985, true, 1, 1),
	('Mega Man X', 1991, true, 2, 3),
    ('Chrono Trigger', 1995, true, 2, 2),
    ('Castlevania: SOTN', 1997, true, 4, 4),
    ('Dr Robotniks Mean Bean Machine', 1993, false, 3, 5);
    
insert into `game_category` values 
	(1, 3), (1, 4), # mario platform/adventure
	(2, 3), (2, 4), # mega man platform/adventure
    (3, 1), 		# chrono trigger rpg
    (4, 1), (4, 3), (4, 4), # SOTN platformer/rpg/adventure
    (5, 5); # dr robotnik puzzle

select g.title, g.release_year, s.`name` as `system`,
	d.`name` as `developer`
from `game` g
join `system` s on g.system_id = s.id
join `developer` d on g.developer_id = d.id
where g.completed
order by g.title;

    

select * from game;

select * from system where id = 1;
*/    