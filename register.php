<?php require_once 'include/header.php' ?>
<?php require_once 'include/nav.php' ?>
<form action="" method="post">
    <fieldset>
        <legend>Create Account</legend>
        <div>
            <label for="username">Username:</label>
            <input type="text" name="username" id="username">
        </div>
        <div>
            <label for="password">Password:</label>
            <input type="password" name="pass" id="pass">
        </div>
        <div>
            <label for="confirm">Confirm password:</label>
            <input type="password" name="confirm" id="confirm">
        </div>
        <div>
            <button type="submit">Save</button>
        </div>
    </fieldset>
</form>
<?php require_once 'include/footer.php' ?>