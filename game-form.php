<?php require_once 'include/header.php' ?>
<?php require_once 'include/nav.php' ?>
<form action="" method="post" enctype="multipart/form-data">
<fieldset>
    <legend>Game form</legend>
    <input type="hidden" name="gameId" value="<?=$model->game->id?>">
    <div>
        <label for="title">Title:</label>
        <input type="text" name="title" id="title"
            value="<?=$model->game->title?>">
    </div>
    <div>
        <label for="year">Release year:</label>
        <input type="text" name="year" id="year"
            value="<?=$model->game->releaseYear?>">
    </div>
    <div>
        <label for="system">System:</label>
        <select name="system" id="system">
            <option value=""></option>
            <?php foreach($model->systems as $system):?>
                <option value="<?=$system->id?>"
                    <?=$model->isSelected($system->id, $model->game->systemId)?>
                >
                    <?=$system->name?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <label for="developer">Developer:</label>
        <select name="developer" id="developer">
            <option value=""></option>
            <?php foreach($model->developers as $developer):?>
                <option value="<?=$developer->id?>"
                    <?=$model->isSelected($developer->id, $model->game->developerId) ?>
                ><?=$developer->name?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <label for="categories">Categories:</label>
        <select name="categories[]" id="categories" multiple>
            <?php foreach($model->categories as $category):?>
                <option value="<?=$category->id?>"
                    <?=$model->isCategorySelected($category->id, $model->game->categoryIds) ?>
                ><?=$category->name?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <label for="completed">
            <input type="checkbox" name="completed" id="completed"
                <?=$model->game->completed ? 'checked' :''?>
            >Completed
        </label>
    </div>
    <div>
        <label for="boxart">Box art:</label>
        <input type="file" name="boxart" id="boxart">
    </div>
    <div>
        <img src="get-image.php?id=<?=$model->game->imageId?>" alt="No box art" height="100">
    </div>
    <div>
        <button type="submit">Save game</button>
    </div>
</fieldset>
</form>
<?php require_once 'include/footer.php' ?>