<?php
require __DIR__ . '/../vendor/autoload.php';

use GameApp\Controllers\GameAppController;
// picks up PHPSESSID session id from the request cookie,
// or creates a new one if not found
session_start();

$controller = new GameAppController();
$model = $controller->resolveRoute();
