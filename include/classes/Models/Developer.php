<?php
namespace GameApp\Models;

class Developer {
    public $id;
    public $name;

    public function __construct(int $id = null, string $name = null) {
        $this->id = $id;
        $this->name = $name;
    }
}
