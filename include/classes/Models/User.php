<?php
namespace GameApp\Models;

class User {
    public $id;
    public $username;
    public $password;

    public function __construct(int $id = null, string $name = null, string $pwd = null) {
        $this->id = $id;
        $this->username = $name;
        $this->password = $pwd;
    }
}
