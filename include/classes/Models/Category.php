<?php
namespace GameApp\Models;

class Category {
    public $id;
    public $name;

    public function __construct(int $id = null, string $name = null) {
        $this->id = $id;
        $this->name = $name;
    }
}
