<?php
namespace GameApp\Models;

class Game {
    public $id;
    public $userId;
    public $title;
    public $releaseYear;
    public $completed;
    public $systemId;
    public $developerId;
    public $categoryIds;
    public $imageId;
    public $systemName;
    public $developerName;
    public $categories;

    public function __construct($id = null, int $userId = null, string $title = null, string $year = null, bool $completed = false, int $sysId = 0, int $devId = 0, array $catIds = [], int $imgId = null) {
        $this->id = $id;
        $this->userId = $userId;
        $this->title = $title;
        $this->releaseYear = $year;
        $this->completed = $completed ? 1 : 0;
        $this->systemId = $sysId;
        $this->developerId = $devId;
        $this->categoryIds = $catIds;
        $this->imageId = $imgId;
    }

    public function isNewGame() : bool {
        return !is_numeric($this->id);
    }

    public function hasRequiredData() : bool {
        return !empty($this->title) && !empty($this->releaseYear) && !empty($this->systemId) && !empty($this->developerId);
    }

    public function getInsertOrUpdateSql() : string {
        if (!$this->isNewGame()) {
            return "UPDATE game SET title=?,release_year=?, system_id=?,developer_id=?, completed=?,
                user_id=? WHERE id = ?";
        }

        return "INSERT INTO game(title,release_year,system_id,developer_id,completed,
            user_id) VALUES(?, ?, ?, ?, ?, ?)";
    }
}
