<?php
namespace GameApp\Controllers;

use GameApp\Services\DataService;
use GameApp\Services\UserService;
use GameApp\Services\GameService;
use GameApp\ViewModels\BaseViewModel;
use GameApp\ViewModels\HomeViewModel;
use GameApp\ViewModels\GameFormViewModel;
use GameApp\ViewModels\LoginViewModel;
use GameApp\ViewModels\RegisterViewModel;
use GameApp\ViewModels\ImageViewModel;

class GameAppController {
    private $dataService;
    private $userService;
    private $gameService;

    public function __construct() {
        $this->dataService = new DataService();
        $this->userService = new UserService($this->dataService);
        $this->gameService = new GameService($this->dataService);
        $this->userService->tryRemember();
    }

    public function resolveRoute() : BaseViewModel {
        preg_match('/\/([-a-z]+)\.php$/', $_SERVER['PHP_SELF'], $matches);

        if (count($matches) !== 2) {
            http_response_code(404);
            echo "Not Found";
            exit();
        }

        $route = $matches[1];
        $loggedIn = $this->userService->isLoggedIn();
        
        switch ($route) {
            case 'index':
                $games = $this->gameService->getGames();
                
                return new HomeViewModel($loggedIn, $games);
            case 'game-form':
                $this->userService->authenticate();

                $game = $this->gameService->getGame();
                $systems = $this->gameService->getSystems();
                $developers = $this->gameService->getDevelopers();
                $categories = $this->gameService->getCategories();
                $model = new GameFormViewModel($loggedIn, $game, $systems, $developers, $categories);
                $model->errorMsg = $this->gameService->handleSubmit();

                return $model;
            case 'delete-game':
                $this->userService->authenticate();

                if (!isset($_GET['id'])) {
                    header('Location: index.php');
                    exit();
                }

                $this->gameService->deleteGame($_GET['id']);
            case 'register':
                $model = new RegisterViewModel($loggedIn);
                $model->errorMsg = $this->userService->registerUser();

                return $model;
            case 'login':
                $model = new LoginViewModel($loggedIn);
                $model->errorMsg = $this->userService->loginUser();
                
                return $model;
            case 'logout':
                return $this->userService->logoutUser();
            case 'get-image':
                return new ImageViewModel($this->dataService);
            default:
                return new BaseViewModel(false);
        }
    }
}
