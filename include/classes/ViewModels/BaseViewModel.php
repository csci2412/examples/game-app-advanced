<?php
namespace GameApp\ViewModels;

class BaseViewModel {
    public $isLoggedIn;
    public $errorMsg;

    public function __construct(bool $loggedIn) {
        $this->isLoggedIn = $loggedIn;
    }
}
