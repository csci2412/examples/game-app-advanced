<?php
namespace GameApp\ViewModels;

class RegisterViewModel extends BaseViewModel {
    public $title = 'Game Form - Register';

    public function __construct(bool $isLoggedIn) {
        parent::__construct($isLoggedIn);
    }
}
