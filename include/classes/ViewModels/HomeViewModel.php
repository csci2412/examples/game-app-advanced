<?php
namespace GameApp\ViewModels;

class HomeViewModel extends BaseViewModel {
    public $title = 'Game Form - Home';
    public $games;

    public function __construct(bool $isLoggedIn, array $games) {
        parent::__construct($isLoggedIn);
        $this->games = $games;
    }
}
