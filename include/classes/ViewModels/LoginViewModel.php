<?php
namespace GameApp\ViewModels;

class LoginViewModel extends BaseViewModel {
    public $title = 'Game Form - Login';

    public function __construct(bool $isLoggedIn) {
        parent::__construct($isLoggedIn);
    }
}
