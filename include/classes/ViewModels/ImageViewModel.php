<?php
namespace GameApp\ViewModels;
use GameApp\Services\DataService;

class ImageViewModel extends BaseViewModel {
    private $dbService;

    public function __construct(DataService $dbService) {
        $this->dbService = $dbService;
    }

    public function getImage() : string {
        $id = $_GET['id'] ?? 0;

        $result = $this->dbService->getResult("SELECT * FROM image WHERE id = $id");

        if (!$result->num_rows) {
            header('Location: index.php');
            exit();
        }

        $image = $result->fetch_object();

        header("Content-Type: $image->filetype");

        return $image->filedata;
    }
}
