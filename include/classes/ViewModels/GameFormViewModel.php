<?php
namespace GameApp\ViewModels;
use GameApp\Models\Game;

class GameFormViewModel extends BaseViewModel {
    public $title = 'Game Form - Game Info';
    public $game;
    public $systems;
    public $developers;
    public $categories;

    public function __construct(bool $isLoggedIn, Game $game, array $systems, array $developers, array $categories) {
        parent::__construct($isLoggedIn);
        $this->game = $game;
        $this->systems = $systems;
        $this->developers = $developers;
        $this->categories = $categories;
    }

    public function isSelected(int $currentId, int $foreignKey) : string {
        return $currentId === $foreignKey ? 'selected' : '';
    }
    
    public function isCategorySelected(int $currentId, array $categoryIds) : string {
        //$categoryIds = explode(',', $categoryIdString);
    
        return in_array($currentId, $categoryIds) ? 'selected' : '';
    }
}
