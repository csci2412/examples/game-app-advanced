<?php
namespace GameApp\Services;

class UserService {
    private $dataService;

    public function __construct(DataService $dataService) {
        $this->dataService = $dataService;
    }

    public function isLoggedIn(): bool {
        return isset($_SESSION['user_id']);
    }
    
    public function authenticate(): void {
        if (!$this->isLoggedIn()) {
            header('Location: login.php');
            exit();
        }
    }

    public function registerUser() {
        // if not POST, don't do anything
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') { 
            return null;
        }

        try {
            // get data from POST
            $username = $_POST['username'];
            $password = $_POST['pass'];
            $confirm = $_POST['confirm'];

            // verify data
            if (empty($username) || empty($password) || empty($confirm)) {
                throw new \Exception('All fields required');
            }

            if ($password !== $confirm) {
                throw new \Exception('Passwords do not match');
            }

            // encrypt password
            $hash = password_hash($password, PASSWORD_BCRYPT);
            $hash = $this->dataService->escapeString($hash);

            // run query
            $sql = "INSERT INTO user (username,password)
                VALUES('$username', '$hash')";
            $result = $this->dataService->getResult($sql);

            if ($result->errno) {
                if ($result->errno === DataService::uniqueErr) { // duplicate key error
                    throw new \Exception('Username already exists');
                }
                throw new \Exception('Registration failed');
            }

            // redirect to login
            header('Location: login.php');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function loginUser() {
        // if not POST, don't do anything
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') { 
            return null;
        }

        try {
            // get data from POST
            $username = $_POST['username'];
            $password = $_POST['pass'];
            $remember = isset($_POST['remember']);

            // validate data
            if (empty($username) || empty($password)) {
                throw new \Exception('All fields required');
            }

            // NOW WITH PREPARED STATEMENTS!
            $stmt = $this->dataService->prepare("SELECT * FROM user WHERE username = ?");

            if (!$stmt) {
                throw new \Exception('Server error');
            }

            $stmt->bind_param('s', $username);

            if (!$stmt->execute()) {
                // log the database error
                $this->dataService->logError($stmt->error);

                throw new \Exception('Problem logging in');
            }

            $result = $stmt->get_result();

            if ($result->num_rows !== 1) {
                throw new \Exception('Username not found');
            }

            // we have a user row
            $user = $result->fetch_object();

            if (!password_verify($password, $user->password)) {
                throw new \Exception('Invalid username or password');
            }

            // login success!
            $_SESSION['user_id'] = $user->id;
            $_SESSION['username'] = $user->username;

            $this->dataService->logInfo("[Login] Username: $user->username");

            if ($remember) {
                $this->setNewRememberToken($user->id);
            }

            // redirect to index
            header('Location: index.php');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function logoutUser() : void {
        $_SESSION = [];

        if (isset($_COOKIE['remember_me'])) {
            list($id) = $this->parseCookie();
            $this->dataService->getResult("delete from token where id = $id");

            // delete cookie (set expiration to time in the past)
            setcookie('remember_me', '', time() - 3600);
        }

        header('Location: index.php');
    }

    /**
     * this function tries using the remember_me cookie to validate
     * user login
     */
    function tryRemember() : void {
        // if we already have user id in the session, or the cookie isn't set, return
        if (isset($_SESSION['user_id']) || !isset($_COOKIE['remember_me'])) {
            return;
        }
        
        // parse the cookie into local variables, use the id to get the token from db
        list($tokenId, $token) = $this->parseCookie();	
        $result = $this->dataService->getResult('select token, user_id from token where id = ' . $tokenId);

        // if no result, return
        if ($result->num_rows !== 1) return;

        $row = $result->fetch_object();

        // if the cookie token doesn't match db token, return
        if ($row->token !== $token) {
            return;
        }

        // get the user record from db based on token user_id
        $userResult = $this->dataService->getResult('select * from user where id = ' . $row->user_id);

        // if no user record, return
        if ($userResult->num_rows !== 1) return;

        // put username in session
        $user = $userResult->fetch_object();
        $_SESSION['user_id'] = $user->id;
        $_SESSION['username'] = $user->username;
        
        // get a new remember_me token (this time update)
        $this->setNewRememberToken($user->id, $tokenId);
    }

    /**
     * this function creates a new token, saves to db (insert or update), and sets a cookie with encoded token val
     */
    private function setNewRememberToken(int $userId, int $tokenId = null) : void {
        // create sha256 hash from random int
        $token = hash('sha256', rand());

        // save the token to the database
        // we could be creating a new token on login or updating a token from "tryRemember"
        if ($tokenId) {
            $stmt = $this->dataService->prepare('update token set token = ? where id = ?');
            $stmt->bind_param('si', $token, $tokenId);
        } else {
            $stmt = $this->dataService->prepare('insert token(token, user_id) values(?,?)');
            $stmt->bind_param('si', $token, $userId);
        }

        // return if there was a problem executing the statement
        if (!$stmt->execute()) {
            $this->dataService->logError($stmt->error);

            return;
        }

        // if not an update, get the new token record id
        if (!$tokenId) {
            $tokenId = $stmt->insert_id;
        }

        // set a base64-encoded cookie called "remember_me" in the format "id|token"
        // set an expiration time of 60 * 60 * 24 * 90 (i.e. 3 months from now)
        setcookie('remember_me', base64_encode($tokenId . '|' . $token), time() + 60 * 60 * 24 * 90);
    }


    /**
     * this function parses the remember_me cookie by base64-decoding, then 
     * exploding the string based on the | character, because the value is 
     * in the form "id|token". Should return [id, token] as an array or
     * an empty array.
     */
    private function parseCookie() : array {
        $vals = explode('|', base64_decode($_COOKIE['remember_me']));

        if (count($vals) !== 2) {
            return [];
        }
        
        return [
            $vals[0] ?? 0,
            $vals[1] ?? '',
        ];
    }
}
