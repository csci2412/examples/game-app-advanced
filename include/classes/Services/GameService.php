<?php
namespace GameApp\Services;
use GameApp\Models\Game;
use GameApp\Models\System;
use GameApp\Models\Developer;
use GameApp\Models\Category;

class GameService {
    private $dataService;
    private const uniqueErr = 1062;
    private const noFileError = 4;

    public function __construct(DataService $dataService) {
        $this->dataService = $dataService;
    }

    public function getGames(): array {
        $userId = $this->getUserId();
        $key = "games";
        $sql = "SELECT g.*, s.name as system, d.name as developer,
            group_concat(c.name) as categories, i.id as image_id, i.filename
        FROM game g 
        JOIN user u on g.user_id = u.id
        JOIN system s on g.system_id = s.id 
        JOIN developer d on g.developer_id = d.id
        LEFT JOIN game_category gc on g.id = gc.game_id
        LEFT JOIN category c on gc.category_id = c.id
        LEFT JOIN image i on i.game_id = g.id
        WHERE u.id = $userId
        GROUP BY g.id
        ORDER BY g.title";

        $resultHandler = function(array $rows) {
            $games = [];

            foreach($rows as $row) {
                $row = (object)$row;
                $game = new Game(
                    $row->id, 
                    $row->user_id, 
                    $row->title, 
                    $row->release_year, 
                    $row->completed, 
                    0,
                    0,
                    [],
                    $row->image_id);
                $game->systemName = $row->system;
                $game->developerName = $row->developer;
                $game->categories = $row->categories;
                array_push($games, $game);
            }

            return $games;
        };

        return $this->dataService->getDataFromCache($sql, $key, $resultHandler);
    }

    public function getGame() : Game {
        if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
            return new Game();
        }
    
        $id = $_GET['id'];
        $key = "games.$id";
        $sql = "SELECT g.*, i.id as image_id, group_concat(category_id) as category_ids
            from game g 
            left join game_category gc on g.id = gc.game_id 
            LEFT JOIN image i on i.game_id = g.id
            where g.id = $id 
            group by g.id, image_id";

        $resultHandler = function(array $rows) {
            if (count($rows) !== 1) return new Game();
            
            $row = (object)$rows[0];
            $game = new Game(
                $row->id, 
                $row->user_id, 
                $row->title, 
                $row->release_year, 
                $row->completed, 
                $row->system_id, 
                $row->developer_id, 
                explode(',', $row->category_ids), 
                $row->image_id);
            
            return $game;
        };
    
        return $this->dataService->getDataFromCache($sql, $key, $resultHandler);
    }
    
    public function getSystems() {
        $sql = "SELECT * FROM system";
        $key = "systems";

        $resultHandler = function(array $rows) {
            $systems = [];

            foreach($rows as $row) {
                array_push($systems, new System($row['id'], $row['name']));
            }

            return $systems;
        };
    
        return $this->dataService->getDataFromCache($sql, $key, $resultHandler);
    }
    
    public function getDevelopers() {
        $sql = "SELECT * FROM developer";
        $key = "developers";

        $resultHandler = function(array $rows) {
            $developers = [];

            foreach($rows as $row) {
                array_push($developers, new Developer($row['id'], $row['name']));
            }

            return $developers;
        };
    
        return $this->dataService->getDataFromCache($sql, $key, $resultHandler);
    }
    
    public function getCategories() {
        $sql = "SELECT * FROM category";
        $key = "categories";

        $resultHandler = function(array $rows) {
            $categories = [];

            foreach($rows as $row) {
                array_push($categories, new Category($row['id'], $row['name']));
            }

            return $categories;
        };
    
        return $this->dataService->getDataFromCache($sql, $key, $resultHandler);
    }
    
    public function handleSubmit() {
        // if not POST, don't do anything
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') { 
            return null;
        }
    
        try {
            $userId = $this->getUserId();
    
            // get data from post array
            $gameId = $_POST['gameId'];
            $title = $_POST['title'];
            $year = $_POST['year'];
            $systemId = $_POST['system'];
            $developerId = $_POST['developer'];
            $categoryIds = $_POST['categories'] ?? []; // array of ids
            $completed = isset($_POST['completed']);

            $game = new Game(
                $gameId,
                $userId,
                $title,
                $year,
                $completed,
                $systemId,
                $developerId,
                $categoryIds,
                $completed
            );
    
            // check that form is filled out
            if (!$game->hasRequiredData()) {
                throw new \Exception('Please enter title, year, system, and developer');
            }
    
            // test if year is numeric
            if (!is_numeric($year)) {
                throw new \Exception('Year must be numeric');
            }
    
            $sql = $game->getInsertOrUpdateSql();
            $stmt = $this->dataService->prepare($sql);

            if (!$stmt) {
                throw new \Exception('Server error');
            }
            
            if ($game->isNewGame()) {
                $stmt->bind_param(
                    'ssiiii',
                    $game->title,
                    $game->releaseYear,
                    $game->systemId,
                    $game->developerId,
                    $game->completed,
                    $game->userId
                );
            } else {
                $stmt->bind_param(
                    'ssiiiii',
                    $game->title,
                    $game->releaseYear,
                    $game->systemId,
                    $game->developerId,
                    $game->completed,
                    $game->userId,
                    $game->id
                );
            }

            if (!$stmt->execute()) {
                $this->dataService->logError($stmt->error);
                
                if ($stmt->errno === self::uniqueErr) { // duplicate key error
                    throw new \Exception("Duplicate entry for title and system");
                }
                throw new \Exception("Save game failed");
            }
    
            // game save was successful
            if ($game->isNewGame()) {
                $game->id = $stmt->insert_id;
            } else {
                $this->dataService->bustCache("games.$game->id");
            }

            // bust cache
            $this->dataService->bustCache("games");
    
            // save game categories
            if (count($categoryIds) > 0) {
                $this->updateGameCategories($game);
            }
    
            // save image to database if one was uploaded
            $this->uploadImage($game->id);
    
            header('Location: index.php');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteGame(int $id) : void {
        $this->dataService->getResult("DELETE FROM game WHERE id = $id");
        $this->dataService->bustCache("games");
        header('Location: index.php');
    }
    
    private function isValidImageType(string $path): bool {
        $acceptedTypes = ['image/jpeg', 'image/png', 'image/gif'];
        $fileInfo = new \finfo(FILEINFO_MIME_TYPE);
    
        return in_array($fileInfo->file($path), $acceptedTypes);
    }
    
    private function uploadImage(int $gameId) : void {
        $boxart = $_FILES['boxart'];
        $name = $boxart['name'];
        $type = $boxart['type'];
        $size = $boxart['size'];
        $error = $boxart['error'];
        $tmp = $boxart['tmp_name'];

        if ($error == self::noFileError) {
            return;
        }
    
        // do we actually have a file to save? if not, bail
        if (!file_exists($tmp) || $error || !$this->isValidImageType($tmp)) {
            throw new \Exception('Problem uploading image');
        }
    
        $stream = fopen($tmp, 'r'); // creates a file stream we can read
        $data = fread($stream, $size); // read data from 0 to length of file
        fclose($stream); // close the file stream now that we've read the data
    
        $data = $this->dataService->escapeString($data);
    
        // see if an image already exists for the game
        $result = $this->dataService->getResult("SELECT * FROM image WHERE game_id = $gameId");
    
        if ($result->num_rows) {
            // update
            $image = $result->fetch_object();
            $this->dataService->getResult("UPDATE image SET filename='$name',filetype='$type',filedata='$data',game_id=$gameId
                WHERE id = $image->id");
        } else {
            // insert
            $this->dataService->getResult("INSERT image (filename,filetype,filedata,game_id) VALUES('$name','$type','$data',$gameId)");
        }
    }
    
    private function updateGameCategories(Game $game) : void {
        // delete any existing rows
        $deleteSql = "DELETE FROM game_category WHERE game_id = $game->id";
        $result = $this->dataService->getResult($deleteSql);
    
        if ($result->errno) {
            return;
        }
    
        // create new rows
        $count = count($game->categoryIds);
        $insertSql = "INSERT INTO game_category VALUES";
    
        foreach($game->categoryIds as $i => $catId) {
            $insertSql .= "($game->id, $catId)";
    
            if ($i + 1 < $count) $insertSql .= ',';
        }
    
        $result = $this->dataService->getResult($insertSql);
    }

    private function getUserId() : int {
        return $_SESSION['user_id'] ?? 0;
    }
}
