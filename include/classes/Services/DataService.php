<?php
namespace GameApp\Services;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use GameApp\Services\EmptyResult;

class DataService {
    private const defaultExp = 60 * 60 * 24;

    private $db;
    private $logger;
    private $cache;

    public function __construct() {
        $this->getConnection();
        $this->getLogger();
        $this->getCache();
    }

    public function escapeString(string $dirty) {
        return $this->db->escape_string($dirty);
    }

    public function logError(string $msg) {
        $this->logger->error($msg);
    }

    public function logInfo(string $msg) {
        $this->logger->info($msg);
    }

    public function bustCache(string $key) {
        $this->cache->delete($key);
    }

    public function getResult(string $selectQuery) : \mysqli_result {
        $result = $this->db->query($selectQuery);
    
        if (is_bool($result)) {
            if (!$result) {
                $this->logError($this->db->error);
            }    
            
            return new EmptyResult($this->db);
        }

        return $result;
    }

    public function prepare(string $query) {
        $stmt = $this->db->prepare($query);

        if (!$stmt) {
            $this->logError($this->db->error);

            return null;
        }

        return $stmt;
    }

    public function getDataFromCache(string $sql, string $key, \Closure $handler, int $exp = self::defaultExp) {
        // check cache
        $data = $this->cache->get($key); // see if data exists for given key
    
        if (!$data) {
            // no data in cache, pull from database
            $result = $this->getResult($sql);
            $rows = $result->fetch_all(MYSQLI_ASSOC); // turn mysqli_result into a standard assoc array
            $data = $handler($rows);
            $this->cache->add($key, serialize($data), $exp); // add data to cache for next time
            $this->logger->info("Adding $key to cache. Result code: {$this->cache->getResultCode()}");
        } else {
            // we found data in the cache
            $data = unserialize($data);
            $this->logger->info("Found $key in cache");
        }
    
        return $data;
    }

    private function getConnection() {
        $this->db = new \mysqli('localhost', 'phpuser','w3lc0m3!','gamedb');
    }

    private function getLogger() {
        $log = new Logger('GameApp');
        $this->logger = $log->pushHandler(new StreamHandler('logs/app.log', Logger::DEBUG));
    }

    private function getCache() {
        $cache = new \Memcached();
        $cache->addServer('localhost', 11211);
        $this->cache = $cache;
    }
}
