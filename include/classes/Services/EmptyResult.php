<?php
namespace GameApp\Services;

class EmptyResult extends \mysqli_result {
    public int $errno;

    public function __construct(\mysqli $db) {
        parent::__construct($db, 0);
        $this->errno = $db->errno;
    }

    public function fetch_object($class_name = null, array $params = null) {
        return false;
    }
}
