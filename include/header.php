<?php require_once 'include/init.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$model->title?></title>
    <style>
        body { font: normal 16px Arial, sans-serif; }
        nav { margin-bottom: 10px; }
        fieldset > div { margin: 10px 0; }
        .error { font-weight: bold; color: red; }
        table { border-collapse: collapse; }
        th, td { border: 1px solid #ccc; padding: 10px; }
        footer {
            text-align: center;
            font-size: 0.8rem;
            margin-top: 60px;
        }
    </style>
</head>
<body>